package cat.copernic.intentprova

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cat.copernic.intentprova.ui.theme.IntentProvaTheme

class SecondActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            IntentProvaTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val receivedText = intent.getStringExtra("textFromActivityA")

                        ActivityBContent(receivedText ?: "No text received")

                }

            }
        }
    }
}

@Composable
fun ActivityBContent(receivedText:String) {
    // Your Compose UI code for ActivityB
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(text = "Activity B")
        Spacer(modifier = Modifier.height(16.dp))
        Text(text = "ReceivedTExt: $receivedText")
    }
}

@Preview(showBackground = true)
@Composable
fun ActivityBContentPreview() {
    IntentProvaTheme {
        ActivityBContent("Texto recibido")
    }
}