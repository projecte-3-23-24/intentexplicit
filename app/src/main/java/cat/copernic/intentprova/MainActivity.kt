package cat.copernic.intentprova

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import cat.copernic.intentprova.ui.theme.IntentProvaTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            IntentProvaTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ActivityAContent()
                }
            }
        }
    }
}

@Composable
fun ActivityAContent() {
    val context = LocalContext.current
    var textToPass by remember { mutableStateOf("") }

    // Your Compose UI code for ActivityA
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(text = "Activity A")
        Spacer(modifier = Modifier.height(16.dp))
        TextField(
            value = textToPass,
            onValueChange = { textToPass = it },
            label = { Text("Text to pass") },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))
        Button(onClick = {
            // Launch ActivityB using Intent when the button is clicked
            val intent = Intent(context,
                SecondActivity::class.java)
            intent.putExtra("textFromActivityA", textToPass)
            context.startActivity(intent) }
        ) {
            Text(text = "Go to Activity B")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ActivityAContentPreview() {
    IntentProvaTheme {
        ActivityAContent()
    }
}